def main():
    """Check palindrome."""
    s = input()
    new_list = s.split()
    print(" ".join([i[::-1] for i in new_list]))


if __name__ == "__main__":
    main()
