"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    n = input()
    answer = 0
    for i in n:
        try:
            int_i = int(i)
            answer += int_i
        except:
            exit()
    print(answer)



if __name__ == "__main__":
    main()
