from mock import patch
import pytest

import strings


@pytest.mark.parametrize("num, expected", [
    ("", "yes"),
    ("1", "yes"),
    ("abc", "no"),
    ("abcba", "yes"),
])
@patch('builtins.input')
def test_main(input_mock, num, expected):
    input_mock.return_value = num
    with patch('builtins.print') as print_mock:
        strings.main()
        print_mock.assert_called_with(expected)
