"""
 Drop empty items from a dictionary.
"""
import json


def main():
    """Drop empty items from a dictionary."""
    try:
        d = json.loads(input())
    except Exception as e:
        print("Please be more accurate, we need a dictionary-like input")
        print("Error:", e)
    list_to_drop = []
    for key,value in d.items():
        if not value:
            list_to_drop.append(key)
    for key in list_to_drop:
        del d[key]
    print(d)



if __name__ == "__main__":
    main()
