"""
Consider a list (list = []). You can perform the following commands:
insert i e: Insert integer e at position i.
print: Print the list.
remove e: Delete the first occurrence of integer e.
append e: Insert integer e at the end of the list.
sort: Sort the list.
pop: Pop the last element from the list.
reverse: Reverse the list.

Initialize your list and read in the value of followed by lines of commands
where each command will be of the  types listed above. Iterate through each command
in order and perform the corresponding operation on your list.
The first line contains an integer, denoting the number of commands.
Each line  of the  subsequent lines contains one of the commands described above.
"""
def insert_cmd(cmd_line, list):
    if len(cmd_line) == 3:
        try:
            i = int(cmd_line[1])
            e = int(cmd_line[2])
            list.insert(i, e)
        except:
            print("Oops, the values were not integer...")
            exit()

def remove_cmd(cmd_line, list):
    if len(cmd_line) == 2:
        try:
            e = int(cmd_line[1])
            list.remove(e)
        except:
            print("Oops, the values were not integer...")
            exit()

def append_cmd(cmd_line, list):
    if len(cmd_line) == 2:
        try:
            e = int(cmd_line[1])
            list.append(e)
        except:
            print("Oops, the values were not integer...")
            exit()

def main():
    """Perform list commands."""
    new_list = []
    try:
        n = int(input())
    except:
        exit()
    for i in range(n):
        command = input()
        command_line = command.split()
        if command_line[0] == "insert":
            insert_cmd(cmd_line=command_line, list=new_list)
        elif command_line[0] == "print":
            print(new_list)
        elif command_line[0] == "remove":
            remove_cmd(cmd_line=command_line, list=new_list)
        elif command_line[0] == "append":
            append_cmd(cmd_line=command_line, list=new_list)
        elif command_line[0] == "sort":
            new_list.sort()
        elif command_line[0] == "pop":
            new_list.pop()
        elif command_line[0] == "reverse":
            new_list.sort(reverse=True)





if __name__ == "__main__":
    main()
